package com.live.elisvobs.bincard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.live.elisvobs.bincard.Item;
import com.live.elisvobs.bincard.R;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    private Context c;
    private ArrayList<Item> items;

    public CustomAdapter(Context c, ArrayList<Item> items) {
        this.c = c;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView = LayoutInflater.from(c).inflate(R.layout.model, parent, false);
        }

        final TextView itemLabel, symbolName, stockBalance;
        itemLabel = convertView.findViewById(R.id.itemName);
        symbolName = convertView.findViewById(R.id.symbolName);
        stockBalance = convertView.findViewById(R.id.stockBalance);

        final Item s = (Item) this.getItem(position);

        itemLabel.setText(s.getItemName());
        symbolName.setText(s.getSymbolName());
        stockBalance.setText(s.getStockBalance());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(c, s.getItemName(), Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }
}
