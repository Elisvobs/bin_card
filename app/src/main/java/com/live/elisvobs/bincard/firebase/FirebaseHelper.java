package com.live.elisvobs.bincard.firebase;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.live.elisvobs.bincard.Item;

import java.util.ArrayList;

public class FirebaseHelper {
    private DatabaseReference db;
    private Boolean saved = null;
    private ArrayList<Item> items = new ArrayList<>();

    //pass database reference
    public FirebaseHelper(DatabaseReference db) {
        this.db = db;
    }

    //write if not null
    public Boolean save(Item item) {
        if (item==null) {
            saved = false;
        } else {
            try {
                db.child("Item").push().setValue(item);
                saved = true;
            } catch (DatabaseException e) {
                e.printStackTrace();
                saved = false;
            }
        }
        return saved;
    }

    //implement fetch data & fill arraylist
    private void fetchData(DataSnapshot dataSnapshot) {
        items.clear();
        for (DataSnapshot ds: dataSnapshot.getChildren()) {
            Item item = ds.getValue(Item.class);
            items.add(item);
        }
    }

    //retrieve

    public ArrayList<Item> retrieve() {
        db.addChildEventListener(new ChildEventListener(){
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return items;
    }
}
