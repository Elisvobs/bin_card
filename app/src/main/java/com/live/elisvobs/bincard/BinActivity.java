package com.live.elisvobs.bincard;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class BinActivity extends AppCompatActivity {
    TextInputEditText date, received, dispatched, balance, initials;
    DatePickerDialog datePickerDialog;
    MaterialButton saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bin);
        ButterKnife.bind(this);

        date = findViewById(R.id.date);
        received = findViewById(R.id.receivedItems);
        dispatched = findViewById(R.id.dispatchedItems);
        balance = findViewById(R.id.itemsBalance);
        initials = findViewById(R.id.initials);

        // perform click event on edit text
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                final int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

                // date picker dialog
                datePickerDialog = new DatePickerDialog(BinActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        },
                        mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }

    @OnClick(R.id.saveButton)
    public void startInfoActivity(View view) {
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }
}
